<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
class AuthController extends Controller
{
    public function register(Request $request)
    {
        $user = new Test;
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }
}
