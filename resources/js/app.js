import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import App from './App.vue';
import Home from './components/Home.vue';
import Register from './components/Register';
import Login from './components/Login';

Vue.use(VueRouter);
Vue.use(Vuetify);
Vue.use(VueAxios ,axios);

axios.defaults.baseURL = 'http://127.0.0.1:8000/api';

const router = new VueRouter({
    routes: [
        { path: '/', name: 'home', component: Home },
        { path: '/register', name: 'register', component: Register },
        { path: '/login' , name: 'login', component: Login },
    ]
});
new Vue({
    el: '#app',
    router: router,
    render: app => app(App)
});