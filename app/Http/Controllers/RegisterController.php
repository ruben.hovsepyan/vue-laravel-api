<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use JWTFactory;
use JWTAuth;
use Validator;

class RegisterController extends Controller
{
    public function __construct()
    {
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),[
           'name'=> 'required' ,
           'surname'=> 'required' ,
           'email'=> 'required' ,
           'password'=> 'required' ,
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        User::create([
            'name' => $request->get('name'),
            'surname' => $request->get('surname'),
            'email' => $request->get('email'),
            'password' =>  Hash::make($request->get('password')),
        ]);
        $user = User::where('email', '=', $request->get('email'))->get();
        $token = JWTAuth::fromUser($user[0]);
        return response()->json(compact('token'));
    }
}
