<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'LoginController@login');
Route::post('/register', 'RegisterController@register');

//Route::middleware('jwt.auth')->post('/logout', 'LogoutController@logout');
Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('/logout', 'LogoutController@logout');
});
Route::middleware('jwt.auth')->get('users', function(Request $request) {
    return auth()->user();
});